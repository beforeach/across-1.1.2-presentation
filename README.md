Sample code accompanying presentation on new features of Across 1.1.2

More information:

* Screencast: https://www.youtube.com/watch?v=YSZUc_AHXqs
* Slides: http://www.slideshare.net/ArneVandamme2/across-112-new-features-february-2016
* Wiki: https://foreach.atlassian.net/wiki/display/AX
