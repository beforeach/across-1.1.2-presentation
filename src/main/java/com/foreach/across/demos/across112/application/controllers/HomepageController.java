package com.foreach.across.demos.across112.application.controllers;

import com.foreach.across.modules.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Arne Vandamme
 */
@Controller
public class HomepageController
{
	@Autowired
	private UserService userService;

	@RequestMapping("/")
	public String homepage( ModelMap model ) {
		model.addAttribute( "users", userService.getUsers() );

		return "th/demo/homepage";
	}
}
