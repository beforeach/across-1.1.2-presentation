package com.foreach.across.demos.across112.application.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.demos.across112.application.installers.config.TestUserGenerator;
import com.foreach.across.modules.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

/**
 * @author Arne Vandamme
 */
@ConditionalOnProperty(name = "test.users")
@Installer(
		name = "testUsersInstaller",
		description = "install some test users",
		phase = InstallerPhase.AfterModuleBootstrap
)
public class TestUsersInstallers
{
	@Value("${test.users}")
	private int numberOfTestUsers;

	@Autowired
	private UserService userService;

	@Autowired
	private TestUserGenerator userGenerator;

	@InstallerMethod
	public void createUsers() {
		userGenerator.generate( numberOfTestUsers ).forEach( userService::save );
	}
}
