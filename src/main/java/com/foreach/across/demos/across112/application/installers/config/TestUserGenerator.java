package com.foreach.across.demos.across112.application.installers.config;

import com.foreach.across.modules.user.business.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Arne Vandamme
 */
@Component
@Lazy
public class TestUserGenerator
{
	public Collection<User> generate( int numberOfUsers ) {
		List<User> users = new ArrayList<>();

		for ( int i = 0; i < numberOfUsers; i++ ) {
			User user = new User();
			user.setUsername( RandomStringUtils.randomAlphanumeric( 25 ) );
			user.setFirstName( user.getUsername() );
			user.setPassword( "test" );

			users.add( user );
		}

		return users;
	}
}
