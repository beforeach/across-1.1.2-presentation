package com.foreach.across.demos.across112;

import com.foreach.across.config.AcrossApplication;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.debugweb.DebugWebModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.user.UserModule;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import javax.sql.DataSource;

/**
 * @author Arne Vandamme
 */
@AcrossApplication(modules = { AdminWebModule.NAME, EntityModule.NAME, UserModule.NAME })
public class DemoApplication
{
	@Bean
	public DataSource acrossDataSource() {
		return new EmbeddedDatabaseBuilder().build();
	}

	@Profile("dev")
	@Bean
	public DebugWebModule debugWebModule() {
		return new DebugWebModule();
	}

	public static void main( String[] args ) {
		SpringApplication.run( DemoApplication.class, args );
	}
}
